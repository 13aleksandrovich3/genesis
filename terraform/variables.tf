variable "vpc_id" {
     type = string
     default = "vpc-0d3d46f420832669f"

}

variable "availability_zone" {
     type = string
     default = "us-east-1a"
}

variable "key_name" {
      type = string
      default = "main"
}
