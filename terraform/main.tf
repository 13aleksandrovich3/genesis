data "aws_ami" "debian_buster" {
   owners = ["136693071363"]
   most_recent = true
   filter {
      name = "name"
      values = ["debian-10-amd64-*"]
   }
}

resource "aws_instance" "prod" {
   ami = data.aws_ami.debian_buster.id
   instance_type = "t2.micro"
   key_name = var.key_name
   availability_zone = var.availability_zone
   vpc_security_group_ids = [aws_security_group.ssh-sg.id]

   lifecycle {
    ignore_changes = ["ami"]
   }
}

resource "aws_ebs_volume" "data" {
   availability_zone = var.availability_zone
   size             = 5
   type             = "gp3"

}

resource "aws_volume_attachment" "ebs_att" {
   device_name = "/dev/sdb"
   volume_id   = aws_ebs_volume.data.id
   instance_id = aws_instance.prod.id
}

resource "aws_eip" "eip" {
   vpc              = true
}

resource "aws_eip_association" "eip_assoc" {
   instance_id   = aws_instance.prod.id
   allocation_id = aws_eip.eip.id
}

resource "aws_security_group" "ssh-sg" {
   name = "ssh-sg"
   ingress {
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      cidr_blocks = ["176.122.90.209/32"]
   }

   egress {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_blocks = ["0.0.0.0/0"]
   }
}

resource "aws_security_group" "web-sg" {
   name = "web-sg"
   ingress {
      from_port   = 80
      to_port     = 80
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
   }
   ingress {
      from_port   = 443
      to_port     = 443
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
   }
   egress {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_blocks = ["0.0.0.0/0"]
   }
}
